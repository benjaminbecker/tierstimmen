console.log('Test')

function playSound(ev, name) {
    ev.preventDefault();
    console.log(name, ev);
    let audioFile = `sounds/${name}.mp3`;
    if (audioFile) {
        console.log('play');
        let audio = new Audio(audioFile);
        audio.play();
    }
}

function addLink(className) {
    let animal = document.getElementsByClassName(className)[0];
    ['mousedown', 'touchstart'].forEach(evName => 
        animal.addEventListener(evName, (ev) => playSound(ev, className))
    );
}


Array.from(document.getElementsByClassName('link'))
.map(e => e.classList[1])
.forEach(className => addLink(className))
